CJP API
=================


Objetivo
-----------------
Tu tarea es escribir una API RESTful con los siguientes requerimientos, por favor lee cuidadosamente y sigue las instrucciones para hacer tu proceso de evaluación más sencillo.


Requerimientos
----------------
El usuario deberá poder:


* Marca (nombre)
    * Crear una nueva marca
    * Obtener una marca por ID
* Categoría (nombre)
    * Crear una nueva categoría
    * Obtener una categoría por ID
* Producto (nombre, precio)
    * Crear un nuevo producto perteneciente a una marca y/o categoría (la marca es obligatoria, la categoría no)
    * Obtener un producto por ID
    * Obtener todos los productos de una marca
    * Obtener todos los productos de una categoría
    * Obtener todos los productos sin categoría


Evaluación
-------------
Publica tu trabajo usando Heroku y comparte la URL de la api con nosotros. De ser posible comparte con nosotros un volcado de la base de datos para replicarla localmente.


Especificaciones Generales
-----------------------------
* Eres libre de usar cualquier paquete, framework o librería mientras puedas justificar su uso.
* Puedes usar cualquier base de datos que desees.
* Sigue las buenas practicas RESTful
* Usa git y crea commits pequeños y significativos para facilitar el proceso de evaluación.
* Incluye un archivo README.md con instrucciones de como instalar y usar tu proyecto localmente para probarlo.
* Sube tu solución a tu cuenta de Github o Bitbucket y comparte el link con nosotros.
* La prueba se diseño con suficiente tiempo para hacer un buen trabajo, toma tu tiempo y procura la calidad.
* De ser posible, procura hacer pruebas unitarias para tu código.


Esperamos saber de ti pronto, disfruta el reto y haz lo mejor que puedas.


¡Mucha Suerte!


CJP Telecom